﻿using System;
using System.Threading.Tasks;
using Calendar.Application.UserAvailability;
using Calendar.Application.Users;
using Microsoft.AspNetCore.Mvc;

namespace Calendar.Api.Controllers
{
    [ApiController]
    [Route("availability")]
    public class AvailabilityController : ControllerBase
    {
        private readonly IAvailabilityRepository _availabilityRepository;
        private readonly ICalendarUserRepository _calendarUserRepository;

        public AvailabilityController(
            IAvailabilityRepository availabilityRepository,
            ICalendarUserRepository calendarUserRepository)
        {
            _availabilityRepository = availabilityRepository;
            _calendarUserRepository = calendarUserRepository;
        }

        [HttpPost]
        public async Task<IActionResult> CreateUserAvailability(
            [FromBody] CreateUserAvailabilityCommand command)
        {
            var handler = new CreateUserAvailabilityCommandHandler(
                _availabilityRepository,
                _calendarUserRepository);
            var result = await handler.HandleCommand(command);
            if (!result.IsValid) return BadRequest(result.Errors);
            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> GetAvailability()
        {
            var query = new GetAvailabilityQuery();
            var handler = new GetAvailabilityQueryHandler(_availabilityRepository);
            var userAvailability = await handler.HandleQuery(query);
            return Ok(userAvailability);
        }


        [HttpGet("{availabilityId:guid}")]
        public async Task<IActionResult> GetAvailabilityById(Guid availabilityId)
        {
            var query = new GetAvailabilityByIdQuery
            {
                AvailabilityId = availabilityId
            };
            var handler = new GetAvailabilityByIdQueryHandler(_availabilityRepository);
            var availability = await handler.HandleQuery(query);
            return Ok(availability);
        }

        [HttpGet("users/{userId:guid}")]
        public async Task<IActionResult> GetAvailabilityByUser(Guid userId)
        {
            var query = new GetUserAvailabilityQuery
            {
                UserId = userId
            };
            var handler = new GetUserAvailabilityQueryHandler(_availabilityRepository);
            var userAvailability = await handler.HandleQuery(query);
            return Ok(userAvailability);
        }
    }
}