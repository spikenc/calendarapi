﻿using System.Linq;
using System.Threading.Tasks;
using Calendar.Api.Models;
using Calendar.Application.Interview;
using Calendar.Application.UserAvailability;
using Calendar.Application.Users;
using Microsoft.AspNetCore.Mvc;

namespace Calendar.Api.Controllers
{
    [ApiController]
    [Route("interview")]
    public class InterviewController : ControllerBase
    {
        private readonly IInterviewRepository _interviewRepository;
        private readonly ICalendarUserRepository _calendarUserRepository;
        private readonly IAvailabilityRepository _availabilityRepository;

        public InterviewController(
            IInterviewRepository interviewRepository,
            ICalendarUserRepository calendarUserRepository,
            IAvailabilityRepository availabilityRepository)
        {
            _interviewRepository = interviewRepository;
            _calendarUserRepository = calendarUserRepository;
            _availabilityRepository = availabilityRepository;
        }
        
        [HttpPost]
        public async Task<IActionResult> RequestInterview(RequestInterviewCommand command)
        {
            var handler = new RequestInterviewCommandHandler(
                _interviewRepository,
                _calendarUserRepository);
            var result = await handler.HandleCommand(command);
            if (!result.IsValid) return BadRequest(result.Errors);
            return Ok();
        }

        [HttpGet("availability")]
        public async Task<IActionResult> GetInterviewAvailability(
            [FromQuery] InterviewAvailabilityQuery query)
        {
            var handler = new InterviewAvailabilityQueryHandler(
                _interviewRepository,
                _calendarUserRepository);
            var availability = await handler.HandleQuery(query);
            var output = availability
                .Select(x => new InterviewTimeSlot
                {
                    Start = x.Start,
                    End = x.End
                });
            return Ok(output);
        }
    }
}