﻿using System;
using System.Threading.Tasks;
using Calendar.Application.Users;
using Microsoft.AspNetCore.Mvc;

namespace Calendar.Api.Controllers
{
    [ApiController]
    [Route("users")]
    public class UsersController : ControllerBase
    {
        private readonly ICalendarUserRepository _calendarUserRepository;

        public UsersController(
            ICalendarUserRepository calendarUserRepository)
        {
            _calendarUserRepository = calendarUserRepository;
        }

        [HttpPost]
        public async Task<IActionResult> CreateUser(CreateUserCommand command)
        {
            var handler = new CreateUserCommandHandler(_calendarUserRepository);
            var result = await handler.HandleCommand(command);
            if (!result.IsValid) return BadRequest(result.Errors);
            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> GetUsers()
        {
            var users = await _calendarUserRepository.GetUsers();
            return Ok(users);
        }

        [HttpGet("{userId:guid}")]
        public async Task<IActionResult> GetUser(Guid userId)
        {
            var query = new GetUserQuery {Id = userId};
            var handler = new GetUserQueryHandler(_calendarUserRepository);
            var user = await handler.HandleQuery(query);
            return Ok(user);
        }
    }
}