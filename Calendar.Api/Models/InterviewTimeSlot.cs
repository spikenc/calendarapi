﻿using System;

namespace Calendar.Api.Models
{
    public class InterviewTimeSlot
    {
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
    }
}