﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Calendar.Domain;

namespace Calendar.Application.Interview
{
    public interface IInterviewRepository
    {
        Task RequestInterview(Domain.CalendarInterview domain);
        Task<IEnumerable<CalendarUserAvailability>> GetAvailableInterviewSlots(
            Guid candidateId,
            IEnumerable<Guid> interviewersId,
            DateTime date);
    }
}