﻿using System;
using System.Collections.Generic;

namespace Calendar.Application.Interview
{
    public class InterviewAvailabilityQuery
    {
        public Guid CandidateId { get; set; }
        public IEnumerable<Guid> InterviewersId { get; set; }
    }
}