﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Calendar.Application.Users;
using Calendar.Domain;

namespace Calendar.Application.Interview
{
    public class InterviewAvailabilityQueryHandler
    {
        private readonly IInterviewRepository _interviewRepository;
        private readonly ICalendarUserRepository _calendarUserRepository;

        public InterviewAvailabilityQueryHandler(
            IInterviewRepository interviewRepository,
            ICalendarUserRepository calendarUserRepository)
        {
            _interviewRepository = interviewRepository;
            _calendarUserRepository = calendarUserRepository;
        }

        public async Task<IEnumerable<CalendarTimeSlot>> HandleQuery(InterviewAvailabilityQuery query)
        {
            var userAvailabilities = await _interviewRepository
                .GetAvailableInterviewSlots(
                    query.CandidateId,
                    query.InterviewersId,
                    DateTime.UtcNow);

            var candidateHasSlots = userAvailabilities.Any(x => x.CalendarUserId == query.CandidateId);
            var interviewersHasSlots = query.InterviewersId.All(x =>
                userAvailabilities.Any(y => y.CalendarUserId == x));
            
            if (candidateHasSlots == false ||
                interviewersHasSlots == false)
                return Enumerable.Empty<CalendarTimeSlot>();

            var candidate = userAvailabilities
                .Where(x => x.CalendarUserId == query.CandidateId)
                .Select(x => new CalendarTimeSlot(x.Start))
                .ToList();

            var interviewers = userAvailabilities
                .Where(x => query.InterviewersId.Contains(x.CalendarUserId))
                .ToList();
            
            var slots = interviewers.ToLookup(
                    x => x.CalendarUserId,
                    x => new CalendarTimeSlot(x.Start))
                .Select(x => x.AsEnumerable())
                .ToArray();

            return CalendarTimeSlotFinder.FindAvailability(
                candidate,
                slots);
        }
    }
}