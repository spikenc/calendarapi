﻿using System;
using System.Collections.Generic;

namespace Calendar.Application.Interview
{
    public class RequestInterviewCommand
    {
        public Guid CandidateId { get; set; }
        public IEnumerable<Guid> InterviewersId { get; set; }
        public DateTime Start { get; set; }
    }
}