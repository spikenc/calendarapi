﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Calendar.Application.Users;
using Calendar.Domain;
using FluentValidation.Results;

namespace Calendar.Application.Interview
{
    public class RequestInterviewCommandHandler
    {
        private readonly IInterviewRepository _interviewRepository;
        private readonly ICalendarUserRepository _calendarUserRepository;

        public RequestInterviewCommandHandler(
            IInterviewRepository interviewRepository,
            ICalendarUserRepository calendarUserRepository)
        {
            _interviewRepository = interviewRepository;
            _calendarUserRepository = calendarUserRepository;
        }

        public async Task<ValidationResult> HandleCommand(RequestInterviewCommand command)
        {
            var validator = new RequestInterviewValidator();
            var validationResult = await validator.ValidateAsync(command);
            if (!validationResult.IsValid) return validationResult;

            var candidate = await _calendarUserRepository.GetUserById(command.CandidateId);
            if (candidate.Role != CalendarUserRole.Candidate)
            {
                validationResult.Errors.Add(new ValidationFailure(
                    nameof(command.CandidateId), "Invalid candidate"));
                return validationResult;
            }

            var interviewers = await _calendarUserRepository.GetUserById(command.InterviewersId);
            if (interviewers.Count() != command.InterviewersId.Count() &&
                interviewers.Any(x => x.Role != CalendarUserRole.Interviewer))
            {
                validationResult.Errors.Add(new ValidationFailure(
                    nameof(command.InterviewersId), "Invalid interviewers"));
                return validationResult;
            }

            var slots = (await _interviewRepository.GetAvailableInterviewSlots(
                    command.CandidateId,
                    command.InterviewersId,
                    DateTime.UtcNow))
                .Where(x => x.Start == command.Start);

            var users = new[] {command.CandidateId}.Concat(command.InterviewersId);
            if (!users.All(x => slots.Any(y => y.CalendarUserId == x)))
            {
                validationResult.Errors.Add(new ValidationFailure(string.Empty,
                    "Unavailable slot"));
                return validationResult;
            }

            var domain = new CalendarInterview
            {
                InterviewId = Guid.NewGuid(),
                TimeSlot = new CalendarTimeSlot(command.Start),
                CandidateId = command.CandidateId,
                InterviewersId = command.InterviewersId
            };

            await _interviewRepository.RequestInterview(domain);
            return validationResult;
        }
    }
}