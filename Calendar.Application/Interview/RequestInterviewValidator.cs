﻿using System;
using FluentValidation;

namespace Calendar.Application.Interview
{
    public class RequestInterviewValidator: AbstractValidator<RequestInterviewCommand>
    {
        public RequestInterviewValidator()
        {
            RuleFor(x => x.Start).GreaterThanOrEqualTo(DateTime.UtcNow);
            RuleFor(x => x.InterviewersId).NotEmpty();
        }
    }
}