﻿using System;

namespace Calendar.Application.UserAvailability
{
    public class CreateUserAvailabilityCommand
    {
        public Guid UserId { get; set; }
        public DateTime Start { get; set; }
    }
}