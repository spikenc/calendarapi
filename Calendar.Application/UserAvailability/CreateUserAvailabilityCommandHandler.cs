﻿using System;
using System.Net.Security;
using System.Threading;
using System.Threading.Tasks;
using Calendar.Application.Users;
using Calendar.Domain;
using FluentValidation.Results;

namespace Calendar.Application.UserAvailability
{
    public class CreateUserAvailabilityCommandHandler
    {
        private readonly IAvailabilityRepository _availabilityRepository;
        private readonly ICalendarUserRepository _calendarUserRepository;

        public CreateUserAvailabilityCommandHandler(
            IAvailabilityRepository availabilityRepository,
            ICalendarUserRepository calendarUserRepository)
        {
            _availabilityRepository = availabilityRepository;
            _calendarUserRepository = calendarUserRepository;
        }

        public async Task<ValidationResult> HandleCommand(CreateUserAvailabilityCommand command)
        {
            if (command == null) throw new ArgumentNullException(nameof(command));
            var validator = new CreateUserAvailabilityCommandValidator();
            var validatorResult = await validator.ValidateAsync(command);
            if (!validatorResult.IsValid) return validatorResult;

            if (!await _calendarUserRepository.UserWithUserIdExists(command.UserId))
            {
                validatorResult.Errors.Add(new ValidationFailure(nameof(command.UserId), "User does not exists"));
                return validatorResult;
            }

            var slot = new CalendarTimeSlot(command.Start);
            await _availabilityRepository.CreateUserAvailability(
                new CalendarUserAvailability
                {
                    Id = Guid.NewGuid(),
                    CalendarUserId = command.UserId,
                    CreatedAt = DateTime.UtcNow,
                    Start = slot.Start,
                    End = slot.End,
                });
            return validatorResult;
        }
    }
}