﻿using System;
using FluentValidation;

namespace Calendar.Application.UserAvailability
{
    public class CreateUserAvailabilityCommandValidator : AbstractValidator<CreateUserAvailabilityCommand>
    {
        public CreateUserAvailabilityCommandValidator()
        {
            RuleFor(x => x.Start).GreaterThanOrEqualTo(DateTime.UtcNow);
            RuleFor(x => x.UserId);
        }
    }
}