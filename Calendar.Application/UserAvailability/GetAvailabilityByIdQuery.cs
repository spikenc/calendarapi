﻿using System;
using System.Collections;

namespace Calendar.Application.UserAvailability
{

    public class GetAvailabilityByIdQuery
    {
        public Guid AvailabilityId { get; set; }
    }
}