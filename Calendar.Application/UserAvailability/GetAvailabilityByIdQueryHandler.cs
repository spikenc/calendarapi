﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Calendar.Domain;

namespace Calendar.Application.UserAvailability
{
    public class GetAvailabilityByIdQueryHandler
    {
        private readonly IAvailabilityRepository _availabilityRepository;

        public GetAvailabilityByIdQueryHandler(
            IAvailabilityRepository availabilityRepository)
        {
            _availabilityRepository = availabilityRepository;
        }

        public async Task<IEnumerable<CalendarUserAvailability>> HandleQuery(GetAvailabilityByIdQuery query)
        {
            return await _availabilityRepository.GetUserAvailabilityById(query.AvailabilityId);
        }
    }
}