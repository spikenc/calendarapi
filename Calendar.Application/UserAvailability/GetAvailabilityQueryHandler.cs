﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Calendar.Domain;

namespace Calendar.Application.UserAvailability
{
    public class GetAvailabilityQueryHandler
    {
        private readonly IAvailabilityRepository _availabilityRepository;

        public GetAvailabilityQueryHandler(
            IAvailabilityRepository availabilityRepository)
        {
            _availabilityRepository = availabilityRepository;
        }

        public async Task<IEnumerable<CalendarUserAvailability>> HandleQuery(
            GetAvailabilityQuery query)
        {
            return await _availabilityRepository.GetUserAvailability();
        }
    }
}