﻿using System;

namespace Calendar.Application.UserAvailability
{
    public class GetUserAvailabilityQuery
    {
        public Guid UserId { get; set; }
    }
}