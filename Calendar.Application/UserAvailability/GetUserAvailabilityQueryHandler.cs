﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Calendar.Domain;

namespace Calendar.Application.UserAvailability
{
    public class GetUserAvailabilityQueryHandler
    {
        private readonly IAvailabilityRepository _availabilityRepository;

        public GetUserAvailabilityQueryHandler(
            IAvailabilityRepository availabilityRepository)
        {
            _availabilityRepository = availabilityRepository;
        }

        public async Task<IEnumerable<CalendarUserAvailability>> HandleQuery(GetUserAvailabilityQuery query)
        {
            return await _availabilityRepository.GetUserAvailabilityByUserId(query.UserId);
        }
    }
}