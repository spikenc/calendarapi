using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Calendar.Domain;

namespace Calendar.Application.UserAvailability
{
    public interface IAvailabilityRepository
    {
        Task<IEnumerable<CalendarUserAvailability>> GetUserAvailability();
        Task<IEnumerable<CalendarUserAvailability>> GetUserAvailabilityById(Guid id);
        Task<IEnumerable<CalendarUserAvailability>> GetUserAvailabilityByUserId(Guid queryUserId);
        Task CreateUserAvailability(CalendarUserAvailability availability);
    }
}