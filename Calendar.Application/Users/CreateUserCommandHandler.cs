﻿using System;
using System.Threading.Tasks;
using Calendar.Domain;
using FluentValidation.Results;

namespace Calendar.Application.Users
{
    public class CreateUserCommandHandler
    {
        private readonly ICalendarUserRepository _userRepository;

        public CreateUserCommandHandler(
            ICalendarUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<ValidationResult> HandleCommand(CreateUserCommand command)
        {
            var validator = new CreateUserCommandValidator();
            var validatorResult = await validator.ValidateAsync(command);
            if (!validatorResult.IsValid) return validatorResult;

            if (await _userRepository.UserWithEmailExists(command.Email))
            {
                validatorResult.Errors.Add(new ValidationFailure(nameof(command.Email), "Email already exists"));
                return validatorResult;
            }

            var domain = new CalendarUser
            {
                Id = command.Id,
                Name = command.Name,
                Email = command.Email,
                Role = command.Role,
                CreatedAt = DateTime.UtcNow
            };

            await _userRepository.CreateUser(domain);
            return validatorResult;
        }
    }
}