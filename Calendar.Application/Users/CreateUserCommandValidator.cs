﻿using System.Linq;
using Calendar.Domain;
using FluentValidation;

namespace Calendar.Application.Users
{
    public class CreateUserCommandValidator : AbstractValidator<CreateUserCommand>
    {
        public CreateUserCommandValidator()
        {
            RuleFor(x => x.Email).EmailAddress();
            RuleFor(x => x.Name).NotEmpty();
            RuleFor(x => x.Role).Must(x =>
                new[] {CalendarUserRole.Candidate, CalendarUserRole.Interviewer}.Contains(x));
        }
    }
}