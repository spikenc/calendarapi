﻿using System;

namespace Calendar.Application.Users
{
    public class GetUserQuery
    {
        public Guid Id { get; set; }
    }
}