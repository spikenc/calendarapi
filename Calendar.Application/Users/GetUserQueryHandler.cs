﻿using System.Threading.Tasks;
using Calendar.Domain;

namespace Calendar.Application.Users
{
    public class GetUserQueryHandler
    {
        private readonly ICalendarUserRepository _calendarUserRepository;

        public GetUserQueryHandler(
            ICalendarUserRepository calendarUserRepository)
        {
            _calendarUserRepository = calendarUserRepository;
        }
        
        public async Task<CalendarUser> HandleQuery(GetUserQuery query)
        {
            return await _calendarUserRepository.GetUserById(query.Id);
        }
    }
}