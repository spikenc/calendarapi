﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Calendar.Domain;

namespace Calendar.Application.Users
{
    public interface ICalendarUserRepository
    {
        Task<bool> UserWithEmailExists(string email);
        Task<bool> UserWithUserIdExists(Guid userId);
        Task<IEnumerable<CalendarUser>> GetUserById(IEnumerable<Guid> ids);
        Task CreateUser(CalendarUser user);
        Task<CalendarUser> GetUserById(Guid id);
        Task<IEnumerable<CalendarUser>> GetUsers();
    }
}