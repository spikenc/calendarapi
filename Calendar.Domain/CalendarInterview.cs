﻿using System;
using System.Collections.Generic;

namespace Calendar.Domain
{
    public class CalendarInterview
    {
        public Guid InterviewId { get; set; }
        public CalendarTimeSlot TimeSlot { get; set; }
        public Guid CandidateId { get; set; }
        public IEnumerable<Guid> InterviewersId { get; set; }
    }
}