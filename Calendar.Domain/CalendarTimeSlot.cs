﻿using System;

namespace Calendar.Domain
{
    public class CalendarTimeSlot
    {
        public DateTime Start { get; }
        public DateTime End { get; }

        public CalendarTimeSlot(DateTime source)
        {
            Start = new DateTime(source.Year, source.Month, source.Day, source.Hour, 0, 0);
            End = Start.AddHours(1);
        }
    }
}