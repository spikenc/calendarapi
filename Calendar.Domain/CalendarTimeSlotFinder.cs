﻿using System.Collections.Generic;
using System.Linq;

namespace Calendar.Domain
{
    public static class CalendarTimeSlotFinder
    {
        private static bool FindSlot(CalendarTimeSlot slot, IEnumerable<CalendarTimeSlot> slots)
        {
            return slots.Any(x => x.Start == slot.Start);
        }

        public static IEnumerable<CalendarTimeSlot> FindAvailability(
            IEnumerable<CalendarTimeSlot> candidateSlots,
            IEnumerable<IEnumerable<CalendarTimeSlot>> interviewersSlots)
        {
            return candidateSlots.Where(candidateSlot =>
                interviewersSlots.Any() &&
                interviewersSlots.All(
                    interviewersSlot => FindSlot(candidateSlot, interviewersSlot)));
        }
    }
}