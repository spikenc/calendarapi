﻿using System;

namespace Calendar.Domain
{
    public class CalendarUserAvailability
    {
        public Guid Id { get; set; }
        public Guid CalendarUserId { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}