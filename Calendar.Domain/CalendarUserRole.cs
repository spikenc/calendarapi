﻿namespace Calendar.Domain
{
    public static class CalendarUserRole
    {
        public const string Candidate = "Candidate";
        public const string Interviewer = "Interviewer";
    }
}