﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Calendar.Application.UserAvailability;
using Calendar.Domain;
using Calendar.Infrastructure.DbContext;
using Microsoft.EntityFrameworkCore;

namespace Calendar.Infrastructure.Availability
{
    public class AvailabilityRepository
        : IAvailabilityRepository
    {
        private readonly CalendarDbContext _context;

        public AvailabilityRepository(CalendarDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<CalendarUserAvailability>> GetUserAvailability()
        {
            return await _context.UserAvailabilities
                .MapToDomain()
                .ToArrayAsync();
        }

        public async Task<IEnumerable<CalendarUserAvailability>> GetUserAvailabilityById(Guid id)
        {
            return await _context.UserAvailabilities
                .ById(id)
                .MapToDomain()
                .ToArrayAsync();
        }

        public async Task<IEnumerable<CalendarUserAvailability>> GetUserAvailabilityByUserId(Guid userId)
        {
            return await _context.UserAvailabilities
                .FilterByUserId(userId)
                .MapToDomain()
                .ToArrayAsync();
        }

        public async Task CreateUserAvailability(CalendarUserAvailability availability)
        {
            _context.UserAvailabilities.Add(new Entities.CalendarUserAvailability
            {
                Id = availability.Id,
                UserId = availability.CalendarUserId,
                StartDate = availability.Start,
                EndDate = availability.End,
                CreatedAt = availability.CreatedAt
            });
            await _context.SaveChangesAsync();
        }
    }
}