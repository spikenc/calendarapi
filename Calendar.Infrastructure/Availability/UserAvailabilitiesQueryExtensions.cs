﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Calendar.Infrastructure.Entities;
using Calendar.Infrastructure.Interview;

namespace Calendar.Infrastructure.Availability
{
    public static class UserAvailabilitiesQueryExtensions
    {
        public static IQueryable<CalendarUserAvailability> ById(
            this IQueryable<CalendarUserAvailability> source,
            Guid id)
        {
            return source.Where(x => x.Id == id);
        }

        public static IQueryable<CalendarUserAvailability> FilterByUserId(
            this IQueryable<CalendarUserAvailability> source,
            Guid id)
        {
            return source.Where(x => x.UserId == id);
        }

        public static IQueryable<CalendarUserAvailability> FilterByUserId(
            this IQueryable<CalendarUserAvailability> source,
            IEnumerable<Guid> id)
        {
            return source.Where(x => id.Contains(x.UserId));
        }

        public static IQueryable<CalendarUserAvailability> NewerThan(
            this IQueryable<CalendarUserAvailability> source,
            DateTime date)
        {
            return source.Where(x => x.StartDate >= date);
        }

        public static IQueryable<Domain.CalendarUserAvailability> MapToDomain(
            this IQueryable<CalendarUserAvailability> source)
        {
            return source.Select(x => new Domain.CalendarUserAvailability
            {
                Id = x.Id,
                CalendarUserId = x.UserId,
                Start = x.StartDate,
                End = x.EndDate,
                CreatedAt = x.CreatedAt
            });
        }

        public static IQueryable<CalendarUserAvailability> CandidateAvailability(
            this IQueryable<CalendarUserAvailability> source,
            IQueryable<InterviewUser> interviewUsers,
            Guid candidateId,
            DateTime date)
        {
            var candidateBookedDates = interviewUsers
                .BookedDates(candidateId);

            return source
                .FilterByUserId(candidateId)
                .NewerThan(date)
                .Where(x => !candidateBookedDates.Contains(x.StartDate));
        }

        public static IQueryable<CalendarUserAvailability> InterviewersAvailability(
            this IQueryable<CalendarUserAvailability> source,
            IQueryable<InterviewUser> interviewUsers,
            IQueryable<CalendarUserAvailability> candidateAvailability,
            IEnumerable<Guid> interviewersId,
            DateTime date
        )
        {
            var interviewersBooked = interviewUsers
                .Where(x => interviewersId.Contains(x.UserId))
                .Select(x => new {x.UserId, x.Interview.Start});

            return source
                .FilterByUserId(interviewersId)
                .NewerThan(date)
                .Where(x =>
                    candidateAvailability.Select(y => y.StartDate).Contains(x.StartDate))
                .Where(x => !interviewersBooked.Any(
                    y => y.Start == x.StartDate &&
                         y.UserId == x.UserId));
        }
    }
}