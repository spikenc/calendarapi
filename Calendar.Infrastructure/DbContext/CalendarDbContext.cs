﻿using Calendar.Infrastructure.Entities;
using Microsoft.EntityFrameworkCore;

namespace Calendar.Infrastructure.DbContext
{
    public class CalendarDbContext : Microsoft.EntityFrameworkCore.DbContext
    {
        private const string DatabaseSchema = "Calendar";

        public DbSet<Entities.CalendarUser> Users { get; set; }
        public DbSet<CalendarUserAvailability> UserAvailabilities { get; set; }
        public DbSet<Entities.Interview> Interviews { get; set; }
        public DbSet<InterviewUser> InterviewUsers { get; set; }

        public CalendarDbContext(DbContextOptions options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema(DatabaseSchema);
            modelBuilder.ApplyConfiguration(new CalendarUserEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new CalendarUserAvailabilityEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new InterviewEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new InterviewUsersEntityTypeConfiguration());
        }
    }
}