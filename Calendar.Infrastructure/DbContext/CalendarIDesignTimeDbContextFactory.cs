﻿using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace Calendar.Infrastructure.DbContext
{
    public class CalendarIDesignTimeDbContextFactory
        : IDesignTimeDbContextFactory<CalendarDbContext>
    {
        public CalendarDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<CalendarDbContext>();
            optionsBuilder.UseSqlite("Data Source=C:\\tmp\\calendar.db");

            return new CalendarDbContext(optionsBuilder.Options);
        }
    }
}