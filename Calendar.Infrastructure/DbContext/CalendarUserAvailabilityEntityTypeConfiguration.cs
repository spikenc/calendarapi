﻿using Calendar.Infrastructure.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Calendar.Infrastructure.DbContext
{
    public class CalendarUserAvailabilityEntityTypeConfiguration
        : IEntityTypeConfiguration<CalendarUserAvailability>
    {
        public void Configure(EntityTypeBuilder<CalendarUserAvailability> builder)
        {
            builder.ToTable("CalendarUserAvailability");
            builder.HasKey(x => x.Id);
            
            builder.Property(x => x.UserId)
                .IsRequired();
            builder.Property(x => x.StartDate)
                .IsRequired();
            builder.Property(x => x.EndDate)
                .IsRequired();
        }
    }
}