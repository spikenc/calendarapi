﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Calendar.Infrastructure.DbContext
{
    public class CalendarUserEntityTypeConfiguration :
        IEntityTypeConfiguration<Entities.CalendarUser>
    {
        public void Configure(EntityTypeBuilder<Entities.CalendarUser> builder)
        {
            builder.ToTable("CalendarUser");
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Name)
                .IsRequired()
                .HasMaxLength(256);

            builder.Property(x => x.Email)
                .IsRequired();

            builder.Property(x => x.Role)
                .IsRequired()
                .HasMaxLength(64);

            builder.Property(x => x.CreatedAt)
                .IsRequired();
        }
    }
}