﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Calendar.Infrastructure.DbContext
{
    public class InterviewEntityTypeConfiguration
        : IEntityTypeConfiguration<Entities.Interview>
    {
        public void Configure(EntityTypeBuilder<Entities.Interview> builder)
        {
            builder.ToTable("Interview");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Start)
                .IsRequired();
            builder.Property(x => x.End)
                .IsRequired();
        }
    }
}