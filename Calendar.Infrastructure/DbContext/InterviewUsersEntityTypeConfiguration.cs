﻿using Calendar.Infrastructure.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Calendar.Infrastructure.DbContext
{
    public class InterviewUsersEntityTypeConfiguration
        : IEntityTypeConfiguration<InterviewUser>
    {
        public void Configure(EntityTypeBuilder<InterviewUser> builder)
        {
            builder.ToTable("InterviewUser");
            builder.HasKey(x => new {x.InterviewId, x.UserId});
            
            builder.HasOne(x => x.Interview)
                .WithMany(x => x.InterviewUser)
                .HasForeignKey(x => x.InterviewId);
            
            builder.HasOne(x => x.User)
                .WithMany(x => x.InterviewUser)
                .HasForeignKey(x => x.UserId);
        }
    }
}