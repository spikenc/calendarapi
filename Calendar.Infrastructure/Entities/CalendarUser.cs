﻿using System;
using System.Collections.Generic;

namespace Calendar.Infrastructure.Entities
{
    public class CalendarUser
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
        public DateTime CreatedAt { get; set; }

        public ICollection<InterviewUser> InterviewUser { get; set; }
    }
}