﻿using System;

namespace Calendar.Infrastructure.Entities
{
    public class CalendarUserAvailability
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}