﻿using System;
using System.Collections.Generic;

namespace Calendar.Infrastructure.Entities
{
    public class Interview
    {
        public Guid Id { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        
        public ICollection<InterviewUser> InterviewUser { get; set; }
    }
}