﻿using System;

namespace Calendar.Infrastructure.Entities
{
    public class InterviewUser
    {
        public Guid InterviewId { get; set; }
        public Interview Interview { get; set; }
        
        public Guid UserId { get; set; }
        public CalendarUser User { get; set; }
    }
}