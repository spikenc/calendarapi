﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Calendar.Application.Interview;
using Calendar.Domain;
using Calendar.Infrastructure.Availability;
using Calendar.Infrastructure.DbContext;
using Calendar.Infrastructure.Entities;
using Microsoft.EntityFrameworkCore;
using CalendarUserAvailability = Calendar.Domain.CalendarUserAvailability;

namespace Calendar.Infrastructure.Interview
{
    public class InterviewRepository
        : IInterviewRepository
    {
        private readonly CalendarDbContext _context;

        public InterviewRepository(
            CalendarDbContext context)
        {
            _context = context;
        }

        public async Task RequestInterview(CalendarInterview domain)
        {
            var entity = new Entities.Interview
            {
                Id = domain.InterviewId,
                Start = domain.TimeSlot.Start,
                End = domain.TimeSlot.End,
                InterviewUser = new List<InterviewUser>
                {
                    new InterviewUser {InterviewId = domain.InterviewId, UserId = domain.CandidateId}
                }
            };

            foreach (var item in domain.InterviewersId)
            {
                entity.InterviewUser.Add(new InterviewUser
                {
                    InterviewId = domain.InterviewId,
                    UserId = item
                });
            }

            _context.Interviews.Add(entity);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<CalendarUserAvailability>> GetAvailableInterviewSlots(
            Guid candidateId,
            IEnumerable<Guid> interviewersId,
            DateTime date)
        {
            var candidateAvailability = _context
                .UserAvailabilities
                .CandidateAvailability(_context.InterviewUsers, candidateId, date);

            var interviewersAvailability = _context.UserAvailabilities
                .InterviewersAvailability(_context.InterviewUsers,
                    candidateAvailability,
                    interviewersId,
                    date);

            return await candidateAvailability
                .Concat(interviewersAvailability)
                .MapToDomain()
                .ToArrayAsync();
        }
    }
}