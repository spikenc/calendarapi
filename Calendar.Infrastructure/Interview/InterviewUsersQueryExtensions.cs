﻿using System;
using System.Linq;
using Calendar.Infrastructure.Entities;

namespace Calendar.Infrastructure.Interview
{
    public static class InterviewUsersQueryExtensions
    {
        public static IQueryable<DateTime> BookedDates(
            this IQueryable<InterviewUser> source,
            Guid userId)
        {
            return source
                .Where(x => x.UserId == userId)
                .Select(x => x.Interview.Start);
        }
    }
}