﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Calendar.Infrastructure.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "Calendar");

            migrationBuilder.CreateTable(
                name: "CalendarUser",
                schema: "Calendar",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    Name = table.Column<string>(type: "TEXT", maxLength: 256, nullable: false),
                    Email = table.Column<string>(type: "TEXT", nullable: false),
                    Role = table.Column<string>(type: "TEXT", maxLength: 64, nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CalendarUser", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CalendarUserAvailability",
                schema: "Calendar",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    UserId = table.Column<Guid>(type: "TEXT", nullable: false),
                    StartDate = table.Column<DateTime>(type: "TEXT", nullable: false),
                    EndDate = table.Column<DateTime>(type: "TEXT", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CalendarUserAvailability", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Interview",
                schema: "Calendar",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    Start = table.Column<DateTime>(type: "TEXT", nullable: false),
                    End = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Interview", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "InterviewUser",
                schema: "Calendar",
                columns: table => new
                {
                    InterviewId = table.Column<Guid>(type: "TEXT", nullable: false),
                    UserId = table.Column<Guid>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InterviewUser", x => new { x.InterviewId, x.UserId });
                    table.ForeignKey(
                        name: "FK_InterviewUser_CalendarUser_UserId",
                        column: x => x.UserId,
                        principalSchema: "Calendar",
                        principalTable: "CalendarUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_InterviewUser_Interview_InterviewId",
                        column: x => x.InterviewId,
                        principalSchema: "Calendar",
                        principalTable: "Interview",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_InterviewUser_UserId",
                schema: "Calendar",
                table: "InterviewUser",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CalendarUserAvailability",
                schema: "Calendar");

            migrationBuilder.DropTable(
                name: "InterviewUser",
                schema: "Calendar");

            migrationBuilder.DropTable(
                name: "CalendarUser",
                schema: "Calendar");

            migrationBuilder.DropTable(
                name: "Interview",
                schema: "Calendar");
        }
    }
}
