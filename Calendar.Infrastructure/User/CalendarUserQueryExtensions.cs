﻿using System;
using System.Collections.Generic;
using System.Linq;
using Calendar.Infrastructure.Entities;

namespace Calendar.Infrastructure.User
{
    public static class CalendarUserQueryExtensions
    {
        public static IQueryable<CalendarUser> FilterById(
            this IQueryable<CalendarUser> source,
            Guid id)
        {
            return source.Where(x => x.Id == id);
        }

        public static IQueryable<CalendarUser> FilterById(
            this IQueryable<CalendarUser> source,
            IEnumerable<Guid> id)
        {
            return source.Where(x => id.Contains(x.Id));
        }


        public static IQueryable<CalendarUser> FilterByEmail(
            this IQueryable<CalendarUser> source,
            string email)
        {
            return source.Where(x => x.Email == email);
        }

        public static IQueryable<Domain.CalendarUser> MapToDomain(this IQueryable<CalendarUser> source)
        {
            return source.Select(x => new Domain.CalendarUser
            {
                Id = x.Id,
                Name = x.Name,
                Email = x.Email,
                Role = x.Role,
                CreatedAt = x.CreatedAt
            });
        }
    }
}