﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Calendar.Application.Users;
using Calendar.Domain;
using Calendar.Infrastructure.DbContext;
using Microsoft.EntityFrameworkCore;

namespace Calendar.Infrastructure.User
{
    public class CalendarUserRepository : ICalendarUserRepository
    {
        private readonly CalendarDbContext _context;

        public CalendarUserRepository(
            CalendarDbContext context)
        {
            _context = context;
        }

        public async Task<bool> UserWithEmailExists(string email)
        {
            return await _context
                .Users
                .FilterByEmail(email).AnyAsync();
        }

        public async Task<bool> UserWithUserIdExists(Guid userId)
        {
            return await _context
                .Users
                .FilterById(userId)
                .AnyAsync();
        }

        public async Task CreateUser(CalendarUser user)
        {
            _context.Users.Add(new Entities.CalendarUser
            {
                Id = user.Id,
                Name = user.Name,
                Email = user.Email,
                Role = user.Role,
                CreatedAt = user.CreatedAt
            });
            await _context.SaveChangesAsync();
        }

        public async Task<CalendarUser> GetUserById(Guid id)
        {
            return await _context
                .Users
                .FilterById(id)
                .MapToDomain()
                .SingleOrDefaultAsync();
        }

        public async Task<IEnumerable<CalendarUser>> GetUserById(IEnumerable<Guid> ids)
        {
            return await _context
                .Users
                .FilterById(ids)
                .MapToDomain()
                .ToArrayAsync();
        }

        public async Task<IEnumerable<CalendarUser>> GetUsers()
        {
            return await _context
                .Users
                .MapToDomain()
                .ToArrayAsync();
        }
    }
}