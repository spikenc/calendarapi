﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Calendar.Api.Models;
using Calendar.Application.Interview;
using Calendar.Application.UserAvailability;
using Calendar.Application.Users;
using Calendar.Infrastructure.DbContext;
using Calendar.Tests.Core;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace Calendar.Tests
{
    [Collection(TestCollection.Integration)]
    public class InterviewTests
        : IClassFixture<ApiWebApplicationFactory>,
            IAsyncLifetime
    {
        private readonly ApiWebApplicationFactory _fixture;
        private readonly HttpClient _client;

        private const string UserResourceUrl = "/users";
        private const string AvailabilityResourceUrl = "/availability";
        private const string InterviewAvailabilityUrl = "/interview/availability";
        private const string InterviewUrl = "/interview";
        private static readonly Guid JohnId = Guid.NewGuid();
        private static readonly Guid MaryId = Guid.NewGuid();
        private static readonly Guid DianaId = Guid.NewGuid();

        public InterviewTests(ApiWebApplicationFactory fixture)
        {
            _fixture = fixture;
            _client = _fixture.CreateClient();
        }

        public async Task InitializeAsync()
        {
            var scope = _fixture.Services.CreateScope();
            var services = scope.ServiceProvider;
            var database = services.GetRequiredService<CalendarDbContext>().Database;
            await database.EnsureDeletedAsync();
            await database.EnsureCreatedAsync();
        }

        public Task DisposeAsync()
        {
            return Task.CompletedTask;
        }

        [Fact]
        public async Task InterviewAvailability()
        {
            var currentDate = DateTime.Today.AddDays(1);

            HttpResponseMessage response;
            foreach (var command in GetCreateUserCommand())
            {
                response = await _client.PostAsJsonAsync(UserResourceUrl, command);
                response.EnsureSuccessStatusCode();
            }

            var availabilityCommand = new List<CreateUserAvailabilityCommand>();
            availabilityCommand.AddRange(GetJohnAvailability(JohnId, currentDate));
            availabilityCommand.AddRange(GetMaryAvailability(MaryId, currentDate));
            availabilityCommand.AddRange(GetDianaAvailability(DianaId, currentDate));

            foreach (var item in availabilityCommand)
            {
                response = await _client.PostAsJsonAsync(AvailabilityResourceUrl, item);
                response.EnsureSuccessStatusCode();
            }

            var queryParameters = new InterviewAvailabilityQuery
            {
                CandidateId = JohnId,
                InterviewersId = new[] {DianaId, MaryId}
            };
            var interviewAvailabilityUrl = GetInterviewAvailabilityUrl(queryParameters);
            var interviewAvailability = await _client.GetFromJsonAsync<IEnumerable<InterviewTimeSlot>>
                (interviewAvailabilityUrl);

            var expectedInterviewAvailability = new[]
            {
                new InterviewTimeSlot
                {
                    Start = currentDate.GetNextWeekday(DayOfWeek.Tuesday).Date.AddHours(9),
                    End = currentDate.GetNextWeekday(DayOfWeek.Tuesday).Date.AddHours(10)
                },
                new InterviewTimeSlot
                {
                    Start = currentDate.GetNextWeekday(DayOfWeek.Thursday).Date.AddHours(9),
                    End = currentDate.GetNextWeekday(DayOfWeek.Thursday).Date.AddHours(10)
                }
            };

            expectedInterviewAvailability
                .All(x => interviewAvailability.Any(y => y.Start == x.Start && y.End == x.End))
                .Should().BeTrue();

            response = await _client.PostAsJsonAsync(InterviewUrl, new RequestInterviewCommand
            {
                Start = expectedInterviewAvailability.First().Start,
                CandidateId = JohnId,
                InterviewersId = new[] {DianaId, MaryId}
            });
            response.EnsureSuccessStatusCode();

            interviewAvailability =
                await _client.GetFromJsonAsync<IEnumerable<InterviewTimeSlot>>(interviewAvailabilityUrl);
            expectedInterviewAvailability.Skip(1)
                .All(x => interviewAvailability.Any(y => y.Start == x.Start && y.End == x.End))
                .Should().BeTrue();
        }

        private static IEnumerable<CreateUserCommand> GetCreateUserCommand()
        {
            return new[]
            {
                new CreateUserCommand
                {
                    Id = JohnId,
                    Name = "John",
                    Email = "john@calendar.io",
                    Role = "Candidate"
                },
                new CreateUserCommand
                {
                    Id = MaryId,
                    Name = "Mary",
                    Email = "mary@calendar.io",
                    Role = "Interviewer"
                },
                new CreateUserCommand
                {
                    Id = DianaId,
                    Name = "Diana",
                    Email = "diana@calendar.io",
                    Role = "Interviewer"
                },
                new CreateUserCommand
                {
                    Id = Guid.NewGuid(),
                    Name = "Peter",
                    Email = "peter@calendar.io",
                    Role = "Candidate"
                }
            };
        }

        private static IEnumerable<CreateUserAvailabilityCommand> GetJohnAvailability(
            Guid userId, DateTime date)
        {
            var dates = new List<DateTime>
            {
                date.GetNextWeekday(DayOfWeek.Monday).Date.AddHours(9),
                date.GetNextWeekday(DayOfWeek.Tuesday).Date.AddHours(9),
                date.GetNextWeekday(DayOfWeek.Wednesday).Date.AddHours(9),
                date.GetNextWeekday(DayOfWeek.Thursday).Date.AddHours(9),
                date.GetNextWeekday(DayOfWeek.Friday).Date.AddHours(9)
            };
            dates.AddRange(DateRange(date.GetNextWeekday(DayOfWeek.Wednesday), 10, 12));
            foreach (var item in dates)
            {
                yield return new CreateUserAvailabilityCommand
                {
                    UserId = userId,
                    Start = item
                };
            }
        }

        private static IEnumerable<CreateUserAvailabilityCommand> GetMaryAvailability(
            Guid userId,
            DateTime date)
        {
            var dates = new List<DateTime>();
            dates.AddRange(DateRange(date.GetNextWeekday(DayOfWeek.Monday), 9, 16));
            dates.AddRange(DateRange(date.GetNextWeekday(DayOfWeek.Tuesday), 9, 16));
            dates.AddRange(DateRange(date.GetNextWeekday(DayOfWeek.Wednesday), 9, 16));
            dates.AddRange(DateRange(date.GetNextWeekday(DayOfWeek.Thursday), 9, 16));
            dates.AddRange(DateRange(date.GetNextWeekday(DayOfWeek.Friday), 9, 16));

            foreach (var item in dates)
            {
                yield return new CreateUserAvailabilityCommand
                {
                    UserId = userId,
                    Start = item
                };
            }
        }

        private static IEnumerable<CreateUserAvailabilityCommand> GetDianaAvailability(
            Guid userId,
            DateTime date)
        {
            var dates = new List<DateTime>();
            dates.AddRange(DateRange(date.GetNextWeekday(DayOfWeek.Monday), 12, 18));
            dates.AddRange(DateRange(date.GetNextWeekday(DayOfWeek.Wednesday), 12, 18));

            dates.AddRange(DateRange(date.GetNextWeekday(DayOfWeek.Tuesday), 9, 12));
            dates.AddRange(DateRange(date.GetNextWeekday(DayOfWeek.Thursday), 9, 12));

            foreach (var item in dates)
            {
                yield return new CreateUserAvailabilityCommand
                {
                    UserId = userId,
                    Start = item
                };
            }
        }

        private static IEnumerable<DateTime> DateRange(DateTime date, int start, int end)
        {
            return Enumerable
                .Range(start, end - start)
                .Select(x => date.Date.AddHours(x));
        }


        private static string InterviewAvailabilityQueryToQueryParameters(
            InterviewAvailabilityQuery source)
        {
            var parameters = new List<(string key, string value)>
            {
                (nameof(source.CandidateId), source.CandidateId.ToString())
            };
            parameters.AddRange(source.InterviewersId.Select(item => (nameof(source.InterviewersId), item.ToString())));

            return string.Join('&',
                parameters.Select(x => $"{WebUtility.UrlEncode(x.key)}= {WebUtility.UrlEncode(x.value)}"));
        }

        private static string GetInterviewAvailabilityUrl(InterviewAvailabilityQuery queryParameters)
        {
            return $"{InterviewAvailabilityUrl}?" + InterviewAvailabilityQueryToQueryParameters(queryParameters);
        }
    }
}