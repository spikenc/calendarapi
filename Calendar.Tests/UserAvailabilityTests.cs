﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Calendar.Application.UserAvailability;
using Calendar.Application.Users;
using Calendar.Infrastructure.DbContext;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace Calendar.Tests
{
    [Collection(TestCollection.Integration)]
    public class UserAvailabilityTests
        : IClassFixture<ApiWebApplicationFactory>,
            IAsyncLifetime
    {
        private readonly ApiWebApplicationFactory _fixture;
        private readonly HttpClient _client;
        private const string AvailabilityResourceUrl = "/availability";
        private const string UsersResourceUrl = "/users";

        public UserAvailabilityTests(ApiWebApplicationFactory fixture)
        {
            _fixture = fixture;
            _client = fixture.CreateClient();
        }

        public async Task InitializeAsync()
        {
            var scope = _fixture.Services.CreateScope();
            var services = scope.ServiceProvider;
            var database = services.GetRequiredService<CalendarDbContext>().Database;
            await database.EnsureDeletedAsync();
            await database.EnsureCreatedAsync();
        }

        public Task DisposeAsync()
        {
            return Task.CompletedTask;
        }

        [Fact]
        public async Task CreateUserAvailability_WhenUserExists_ReturnsOK()
        {
            var userId = Guid.NewGuid();
            var response = await _client.PostAsJsonAsync(
                UsersResourceUrl,
                new CreateUserCommand
                {
                    Id = userId,
                    Name = "John",
                    Email = "john@calendar.io",
                    Role = "Candidate"
                });
            response.EnsureSuccessStatusCode();

            response = await _client.PostAsJsonAsync(
                AvailabilityResourceUrl,
                new CreateUserAvailabilityCommand
                {
                    UserId = userId,
                    Start = DateTime.Today.AddDays(1).AddHours(9),
                });
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Fact]
        public async Task CreateUserAvailability_WhenDoesUserExists_ReturnsBadRequest()
        {
            var response = await _client.PostAsJsonAsync(AvailabilityResourceUrl,
                new CreateUserAvailabilityCommand
                {
                    UserId = Guid.NewGuid(),
                    Start = DateTime.Today.AddDays(1).AddHours(9),
                });
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        [Fact]
        public async Task CreateUserAvailability_WhenDateIsInThePast_ReturnsBadRequest()
        {
            var userId = Guid.NewGuid();
            var response = await _client.PostAsJsonAsync(UsersResourceUrl,
                new CreateUserCommand
                {
                    Id = userId,
                    Name = "John",
                    Email = "john@calendar.io",
                    Role = "Candidate"
                });
            response.EnsureSuccessStatusCode();

            response = await _client.PostAsJsonAsync(AvailabilityResourceUrl,
                new CreateUserAvailabilityCommand
                {
                    UserId = userId,
                    Start = DateTime.Today.AddDays(-10).AddHours(9),
                });
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }
    }
}