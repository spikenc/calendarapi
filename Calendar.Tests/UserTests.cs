﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Calendar.Application.Users;
using Calendar.Infrastructure.DbContext;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace Calendar.Tests
{
    [Collection(TestCollection.Integration)]
    public class UserTests
        : IClassFixture<ApiWebApplicationFactory>,
            IAsyncLifetime
    {
        private readonly ApiWebApplicationFactory _fixture;
        private readonly HttpClient _client;
        private const string UsersResourceUrl = "/users";

        public UserTests(ApiWebApplicationFactory fixture)
        {
            _fixture = fixture;
            _client = fixture.CreateClient();
        }

        public async Task InitializeAsync()
        {
            var scope = _fixture.Services.CreateScope();
            var services = scope.ServiceProvider;
            var database = services.GetRequiredService<CalendarDbContext>().Database;
            await database.EnsureDeletedAsync();
            await database.EnsureCreatedAsync();
        }

        public Task DisposeAsync()
        {
            return Task.CompletedTask;
        }

        [Fact]
        public async Task CreateUser_WhenUserDoesNotExists_ReturnsOK()
        {
            var response = await _client.PostAsJsonAsync(UsersResourceUrl,
                new CreateUserCommand
                {
                    Id = Guid.NewGuid(),
                    Name = "John",
                    Email = "john@calendar.io",
                    Role = "Candidate"
                });
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Fact]
        public async Task CreateUser_WhenUserHasInvalidRole_ReturnsBadRequest()
        {
            var response = await _client.PostAsJsonAsync(UsersResourceUrl,
                new CreateUserCommand
                {
                    Id = Guid.NewGuid(),
                    Name = "John",
                    Email = "john@calendar.io",
                    Role = "invalid"
                });
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }
    }
}